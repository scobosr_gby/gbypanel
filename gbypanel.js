// ==UserScript==
// @name       Control Panel for Grability RMS
// @namespace  GBYPANEL
// @version    0.07
// @description  Control Panel for Grability RMS
// @include      https://cms-new-eci.app/rms/*
// @include      *.elcorteingles.es/rms/*
// @include      *.elcorteingles.pt/rms/*
// @include      *.grability.elcorteingles.es/rms/*
// @include      *.grability.elcorteingles.pt/rms/*
// @include      *.grabilityops.com/eci-essentials/cms-eci-new/public/*
// @include      *.ecinuevo.com/rms/*
// @author       Sergio Cobos Ruiz
// @copyright    2019, Sergio Cobos Ruiz
// @updateURL https://bitbucket.org/scobosr_gby/gbypanel/src/master/gbypanel.js
// @require http://code.jquery.com/jquery-latest.js
// @require https://code.jquery.com/ui/1.12.1/jquery-ui.js
// @require https://cdnjs.cloudflare.com/ajax/libs/stupidtable/1.1.3/stupidtable.js

// ==/UserScript==

$(document).ready(function() {
    // Contenido de espacios
    var sqlitesTable = '<table class="table" id="sqlitesTable" style="font-family: arial, sans-serif; border-collapse: collapse; width: 100%;"> <thead> <tr> <th data-sort="string" scope="col" id="store_id" style="border: 1px solid #dddddd; text-align: left; padding: 8px;">store_id</th> <th data-sort="date" scope="col" id="tb" style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Tablet Date</th> <th data-sort="date" scope="col" id="sp" style="border: 1px solid #dddddd; text-align: left; padding: 8px;">Smartphone Date</th> </tr> </thead> <tbody> </tbody>';
    var aditionRow = '<td style="border: 1px solid #dddddd; text-align: center; padding: 8px;"> <select name="corridorsListOrigin" id="corridorsListOrigin"> <br> <option>-------Selecciona pasillo de origen------</option> </select> </td> <td style="border: 1px solid #dddddd; text-align: center; padding: 8px;"> <select name="subcorridorsListOrigin" id="subcorridorsListOrigin"> <option>-------Selecciona subpasillo de origen------</option> </select> </td> <td style="border: 1px solid #dddddd; text-align: center; padding: 8px;"> <select name="regularTemplateList" id="regularTemplateList"> <option>N/A</option> </select> </td> <td style="border: 1px solid #dddddd; text-align: center; padding: 8px;"> <select name="basculeTemplateList" id="basculeTemplateList"> <option>N/A</option> </select> </td> <td style="border: 1px solid #dddddd; text-align: center; padding: 8px;"> <input type="button" value="Migrar" id="executeMigrationSubcorridor"> </td> </tr>';
    var storeMigration = '<div style="text-align: center;"> <p>Selecciona un país y dispositivo de origen (este será utilizado tambien para el destino)</p> <select name="countryList" id="selectCountryMigration"> <option>-------Selecciona un país------</option> <option value=".es">España</option> <option value=".pt">Portugal</option> </select> <select name="devicesList" id="selectDeviceMigration"> <option>-------Selecciona un dispositivo------</option> <option value="1">Tablet</option> <option value="2">Smartphone</option> </select> </div> <br> <table id="storeMigrationTable" width: 100%;"> <thead> <tr> <th style="border: 1px solid #dddddd; text-align: center; padding: 8px;">Pasillo Origen</th> <th style="border: 1px solid #dddddd; text-align: center; padding: 8px;">Subpasillo Origen</th> <th style="border: 1px solid #dddddd; text-align: center; padding: 8px;"> <p> Plantilla Regular Destino </p> </th> <th style="border: 1px solid #dddddd; text-align: center; padding: 8px;"> <p> Plantilla Pesable Destino </p> </th> </tr> </thead> <tbody>' + aditionRow + '</tbody>';
    var contentTabs = '<div id="tabs"> <ul> <li><a href="#tabs-1">Reubicar Subpasillos</a></li> <li><a href="#tabs-2">Reubicar Antojos</a></li> <li><a href="#tabs-3">Migración de tienda</a></li> <li><a href="#tabs-4">Listado SQLites</a></li> </ul> <div id="tabs-1"> <p>Centros:</p> <select name="storesList" id="selectStores"> <option>-------Selecciona un centro/s en el que reubicar------</option> <option value="all">Todos los centros</option> </select> <p>Dispositivo:</p> <select name="devicesList" id="selectDevice"> <option>-------Selecciona un dispositivo------</option> <option value="1">Tablet</option> <option value="2">Smartphone</option> </select> <p>Pasillo:</p> <select name="corridorsList" id="selectCorridor"> <option>-------Selecciona un pasillo------</option> </select> <p>Subpasillo:</p> <select name="subcorridorsList" id="selectSubcorridor"> <option>-------Selecciona un Subpasillo------</option> </select> <div style="text-align: center;"> <p>------------------------------------------------------</p> </div> <div class="sorting-regular-exec" style="text-align: center;"> <input type="button" value="Reubicar" id="executeSorting"> <img class="hide" id="status-loading" src="http://icon-park.com/imagefiles/loading7_green.gif" style="width: 10%; height: 10%;"> <img class="hide" id="status-ok" src="http://icons.iconarchive.com/icons/custom-icon-design/flatastic-9/128/Accept-icon.png" style="width: 10%; height: 10%;"> <img class="hide" id="status-ko" src="https://www.freeiconspng.com/uploads/error-icon-4.png" style="width: 10%; height: 10%;"> </div> </div> <div id="tabs-2"> <p>Centros:</p> <select name="storesListWhims" id="selectStoresWhims"> <option>-------Selecciona un centro/s en el que reubicar------</option> <option value="all">Todos los centros</option> </select> <div style="text-align: center;"> <p>------------------------------------------------------</p> </div> <div class="sorting-regular-exec" style="text-align: center;"> <input type="button" value="Reubicar" id="executeSortingWhims"> <img class="hide" id="statusWhims-loading" src="http://icon-park.com/imagefiles/loading7_green.gif" style="width: 10%; height: 10%;"> <img class="hide" id="statusWhims-ok" src="http://icons.iconarchive.com/icons/custom-icon-design/flatastic-9/128/Accept-icon.png" style="width: 10%; height: 10%;"> <img class="hide" id="statusWhims-ko" src="https://www.freeiconspng.com/uploads/error-icon-4.png" style="width: 10%; height: 10%;"> </div> </div> <div id="tabs-3"> ' + storeMigration + ' </div> <div id="tabs-4"> ' + /*sqlitesTable*/ + ' </div> </div>';
    // Configuracion base
    function getConfig() {
        var host = document.location.origin;
        return host;
    }
    function getCountry() {
        var domain = getConfig();
        var country = domain.includes('.pt') == true ? 'pt' : 'es';
        console.log(country);
        return country;
    }
    // Configuracion adicional si es el entorno de shared
    function getConfigShared() {
        var host = document.location.origin;
        if (host == 'https://shared.grabilityops.com') {
            return '/eci-essentials/cms-eci-new/public';
        } else {
            return '';
        }
    }
    // Enviar path param como query param a cms-new-eci
    function getQueryParam(pathTo, destiny = false) {
        var host = getConfig();
        var country = jQuery('#selectCountryMigration option:selected').val();
        if (host == 'https://cms-new-eci.app' || host == 'http://cms-new-eci.app') {
            return '/api/redirecttoprodget?country=' + country + '&destiny=' + destiny + '&pathTo=' + pathTo;
        } else {
            return pathTo;
        }
    }
    // Obtener plantillas de PRO por medio de cms-new-eci
    function getTemplatesForDevice() {
        var device = jQuery('#selectDeviceMigration option:selected').val() == '1' ? 'tablet' : 'smartphone';
        var path = '/api/' + device + '/structures/all';
        var url = getConfig() + getConfigShared() + getQueryParam(path, true);
        console.log(url);
        var templatesList = getData(url);

        putOptions('regularTemplateList', templatesList, 'name');
        putOptions('basculeTemplateList', templatesList, 'name');
    }
    // Centros
    function getSqlitesInfo() {
        var devices = ['1', '2'];
        var centros = ["10011", "10730", "10010", "10731", "10013", "10012", "10732", "10582", "10901", "10737", "10019", "10018", "10097", "10733", "10734", "10906", "10814", "10718", "10716", "10970", "10705", "10704", "10707", "10706", "10907", "10701", "10783", "10786", "10647", "10785", "10029", "10784", "10581", "10703", "10020", "10702", "10002", "10915", "10916", "10001", "10913", "10006", "10911", "10005", "10719", "10004", "10003", "10009", "10008", "10007", "10722", "10723", "10710", "10098", "10015", "10014", "10709", "10017", "10900", "10714", "10713", "10864", "10989", "10712", "10711", "10973", "10974", "10087", "10697", "10695", "10692", "10027", "10025", "10023", "10022", "10021", "10941", "10729", "10726", "10727", "10725", "10720", "10721", "10599", "10728", "10735", "10736", "10246", "10036", "10975"];
        var sqlitesResult = [];

        centros.forEach(centro => {
            var tablet;
            var smartphone;
            devices.forEach(device => {
                var sqliteUrl = getConfig() + getConfigShared() + '/api/versions/published/' + device + '/' + centro + '/?club_gourmet=false';
                var sqliteDataInfo = getData(sqliteUrl);
                device == '1' ? tablet = sqliteDataInfo.timestamp : smartphone = sqliteDataInfo.timestamp;
            });
            var myHtmlContent = "<td scope='row' style='border: 1px solid #dddddd; text-align: left; padding: 8px;'>'" + centro + "'</td> <td>" + tablet + "</td> <td>" + smartphone + "</td> </tr>";
            var tableRef = document.getElementById('sqlitesTable').getElementsByTagName('tbody')[0];
            var newRow   = tableRef.insertRow(tableRef.rows.length);
            newRow.innerHTML = myHtmlContent;
        });
    }
    // SQLites Info
    function getCenters() {
        //getSqlitesInfo();
        var stores;
        var centrosECI = getData(getConfig() + getConfigShared() + getQueryParam('/api/product-sorting/store-ids/eci'));
        if (getCountry() == 'es') {
            var centrosHIPER = getData(getConfig() + getConfigShared() + getQueryParam('/api/product-sorting/store-ids/hipercor'));
            var centros = centrosECI.concat(centrosHIPER);
            stores = centros;
        } else {
            stores = centrosECI;
        }
        console.log(stores);
        window.localStorage.setItem('centres', JSON.stringify(stores));
        putOptions('selectStores', stores);
        putOptions('selectStoresWhims', stores);
    }
    // Pasillos
    function getCorridors(origin = '#selectDevice option:selected', destiny = 'selectCorridor') {
        var originIdElement = origin;
        var destinyIdElement = destiny;
        var device = jQuery(originIdElement).val() == '1' ? 'tablet' : 'smartphone';
        var countrySelectedForMigration = jQuery('#selectCountryMigration option:selected').val();
        var countrySelectedForSorting = getCountry();
        var realPath = '';
        if (countrySelectedForMigration == '.es' || countrySelectedForSorting == 'es') {
            realPath = '/api/eci/supermarket/' + device + '/corridors';
        } else {
            realPath = '/api/portugal/supermarket/' + device + '/corridors';
        }
        var url = getConfig() + getConfigShared() + getQueryParam(realPath);
        console.log(url);
        var corridorsList = getData(url);
        cleanOptions(destinyIdElement);
        putOptions(destinyIdElement, corridorsList, 'corridor_id');
    }
    // Subpasillos
    function getSubCorridors(deviceOrigin = '#selectDevice option:selected', origin = '#selectCorridor option:selected', destiny = 'selectSubcorridor') {
        var originIdElement = origin;
        var destinyIdElement = destiny;
        var corridor_id = jQuery(originIdElement).val();
        var device = jQuery(deviceOrigin).val() == '1' ? 'tablet' : 'smartphone';
        var realPath = '';
        if (getCountry() == 'es' || jQuery('#selectCountryMigration option:selected').val() == '.es') {
            realPath = '/api/eci/supermarket/' + device + '/corridors/' + corridor_id + '/subcorridors';
        } else {
            realPath = '/api/portugal/supermarket/' + device + '/corridors/' + corridor_id + '/subcorridors';
        }
        var url = getConfig() + getConfigShared() + getQueryParam(realPath);
        console.log(url);
        var subCorridorsList = getData(url);
        console.log(subCorridorsList);
        cleanOptions(destinyIdElement);
        putOptions(destinyIdElement, subCorridorsList);
    }
    // SortingJobs
    function getSortingJobs() {
        var subcorridor_name = jQuery('#selectSubcorridor option:selected').attr("name");
        var url = getConfig() + getConfigShared() + getQueryParam('/api/product-sorting/all-sortingjobs/eci');
        console.log(url);
        var jobsList = getData(url);
        var jobsFiltered = {'tablet': [], 'smartphone': []};
        jobsList.forEach(sortingJob => {
            var device = sortingJob['device'] == 1 ? 'tablet' : 'smartphone';
            var subcorridorNamelower = sortingJob.subcorridor.toLowerCase();
            jobsFiltered[device].push(subcorridorNamelower + '-id-' + sortingJob['job']);
        });
        console.log(jobsFiltered);
        return jobsFiltered;
    }
    function makeRequestSortingJob(url, centerSelected) {
        var data = $.ajax({ type: "POST",
                           beforeSend: function(request) {
                               request.setRequestHeader("Content-Type", 'application/json');
                           },
                           data: JSON.stringify([centerSelected]),
                           dataType: "json",
                           url: url,
                           async: true,
                           success:function(success) {
                               $("#status-loading").css("display", "none");
                               $("#status-ok").css("display", "inline-block");
                           },
                           error: function(error){
                               $("#status-loading").css("display", "none");
                               $("#status-ko").css("display", "inline-block");
                           }
                          }).responseJSON;
        console.log('result of request');
        console.log(data);
        //return data;
    }
    function makeRequestSortingWhims(url, centerSelected) {
        var data = $.ajax({ type: "POST",
                           beforeSend: function(request) {
                               request.setRequestHeader("Content-Type", 'application/json');
                           },
                           //data: JSON.stringify([centerSelected]),
                           dataType: "json",
                           url: url,
                           async: true,
                           success:function(success) {
                               $("#statusWhims-loading").css("display", "none");
                               $("#statusWhims-ok").css("display", "inline-block");
                           },
                           error: function(error){
                               $("#statusWhims-loading").css("display", "none");
                               $("#statusWhims-ko").css("display", "inline-block");
                           }
                          }).responseJSON;
        console.log('result of request');
        console.log(data);
        //return data;
    }
    // Funciones
    function getData(url) {
        var data = $.ajax({ type: "GET",
                           url: url,
                           async: false
                          }).responseJSON;
        console.log(data);
        return data;
    }
    function postDataSorting(type) {
        var option_selected = type == 'regular' ? "#selectStores option:selected" : "#selectStoresWhims option:selected";
        var centerSelected = jQuery(option_selected).val();

        if (type == 'regular') {
            var deviceName = jQuery("#selectDevice option:selected").val() == '1' ? 'tablet' : 'smartphone';
            var subcorridorSelected = jQuery('#selectSubcorridor option:selected').text();
            var jobs = getSortingJobs();
            var corridorName = jQuery('#selectSubcorridor option:selected').attr("name");
            console.log(subcorridorSelected);
            var subcorridorNamelower = subcorridorSelected.toLowerCase();
            var jobId = findSubcorridorJob(jobs[deviceName], subcorridorNamelower);
            console.log('job_id its :'+jobId);
            var url = getConfig() + getConfigShared() + '/api/product-sorting/sortingjobs/initJobForStores/'+ jobId;
            console.log(url);
            if (centerSelected == 'all') {
                var centres = JSON.parse(window.localStorage.getItem('centres'));
                var centersForRequest = [];
                centres.forEach(centre => {
                    centersForRequest.push(centre.id)
                    console.log(centre.id);
                });
                console.log(centersForRequest);
                makeRequestSortingJob(url, centersForRequest);
            } else {
                makeRequestSortingJob(url, centerSelected);
            }
        } else {
            var preUrl = getConfig() + getConfigShared() + '/api/product-sorting/sortingjobs/sortwhims/';
            if (centerSelected == 'all') {
                var centres = JSON.parse(window.localStorage.getItem('centres'));
                var centersForRequest = [];
                centres.forEach(centre => {
                    centersForRequest.push(centre.id)
                    console.log(centre.id);
                    console.log(centersForRequest);
                    var urlMultiWhim = preUrl + centre.id;
                    console.log('urlMultiWhim ' + urlMultiWhim);
                    makeRequestSortingWhims(urlMultiWhim, centersForRequest);
                });
            } else {
                var urlOneWhim = preUrl + centerSelected;
                console.log('urlOneWhim ' + urlOneWhim);
                makeRequestSortingWhims(urlOneWhim, centerSelected);
            }
        }


    }
    function putOptions(list, options, id) {
        var trueId = '';
        if (id == 'corridor_id' || id == 'id') {
            trueId = id == 'corridor_id' ? 'corridor_id' : 'id';
        } else {
            trueId = id;
        }
        var select = document.getElementById(list);
        options.forEach(element => {
            var option = document.createElement("option");
            option.innerHTML = element['name'];
            option.value = element[trueId];
            select.appendChild(option);
        });
    }
    function cleanOptions(list) {
        var i;
        var selectbox = document.getElementById(list);
        for(i = selectbox.options.length - 1 ; i >= 0 ; i--)
        {
            selectbox.remove(i);
        };
        var option = document.createElement("option");
        option.innerHTML = list == 'selectSubcorridor' ? '-------Selecciona un Subpasillo------': '-------Selecciona un pasillo------';
        selectbox.appendChild(option);
    }
    function findSubcorridorJob(deviceJobs, subcorridorName) {
        var jobId = '';
        var forReplace = subcorridorName + '-id-';
        deviceJobs.forEach(sortingJob => {
            if (sortingJob.indexOf(subcorridorName) === 0) {
                jobId = sortingJob;
            }
        });
        console.log(jobId);
        console.log(jobId.replace(forReplace, ''));
        return jobId.replace(forReplace, '');
    }


    $( function() {
        $("sqlitesTable").stupidtable();

        $( "#tabs" ).tabs();
        $( "#dialogCP" ).dialog({
            closeOnEscape: true,
            closeText: "",
            minWidth: 1300,
            resizable: false,
            draggable: false,
            autoOpen: false,
            show: {
                effect: "blind",
                duration: 300
            },
            hide: {
                effect: "blind",
                duration: 300
            }
        });

        $( "#CP" ).on( "click", function() {
            $("#status-loading").css("display", "none");
            $("#status-ok").css("display", "none");
            $("#status-ko").css("display", "none");
            $("#statusWhims-loading").css("display", "none");
            $("#statusWhims-ok").css("display", "none");
            $("#statusWhims-ko").css("display", "none");
            $( "#dialogCP" ).dialog( "open" );
            getCenters();
        });
        $('#selectDevice').change(function(){
            getCorridors();
        });
        $('#selectDeviceMigration').change(function(){
            getCorridors('#selectDeviceMigration', 'corridorsListOrigin');
            getTemplatesForDevice();
        });
        $('#selectCorridor').change(function(){
            getSubCorridors();
        });
        $('#corridorsListOrigin').change(function(){
            getSubCorridors('#selectDeviceMigration', '#corridorsListOrigin option:selected', 'subcorridorsListOrigin');
        });
        $( "#executeSorting" ).on( "click", function() {
            $("#status-loading").css("display", "inline-block");
            $("#status-ok").css("display", "none");
            $("#status-ko").css("display", "none");
            postDataSorting('regular');
        });
        $( "#executeSortingWhims" ).on( "click", function() {
            $("#statusWhims-loading").css("display", "inline-block");
            $("#statusWhims-ok").css("display", "none");
            $("#statusWhims-ko").css("display", "none");
            postDataSorting('whims');
        });
    } );

    $('body').append('<input type="button" value="Control Panel" id="CP">')
    $('body').append('<div id="dialogCP" title="Control Panel">'+contentTabs+'</div>')
    $("#CP").css("position", "fixed").css("top", "40%").css("left", 0);
});